//2.2 variant
//  ViewController.swift
//  l2_dz2
//
//  Created by Hen Joy on 4/9/19.
//  Copyright © 2019 Hen Joy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        workWithFunctionReturn()
        
    }
    
    func workWithFunctionReturn(){
        let firstYear = 1826
        let currentYear = 2019
        let sum = 24.0
        let scholarshipStudent = 700.0
        let costs = 1000.0
        let savings = 2400.0
        let reverseDig = Int.random(in: 11...100)
        var result = 0.0
        result = task1ToCountSum(fromYear: firstYear, toYear: currentYear,summa: sum)
        print("Sum with percentage from \(firstYear) till \(currentYear) = \(result)")
        result = task2ToCountSumToLiveUptoNextYear(scholarship: scholarshipStudent, cosT: costs)
        print("Money to live up to end of year = \(result)")
        result = task3ToCountMonths(scholarship: scholarshipStudent, cosT: costs, saving: savings)
        print("Money is enough to live \(result) months")
        let res = task4ReverseDigit(reversedDigit: reverseDig)
        print("Before reverse = \(reverseDig)")
        print("Reversed digital = \(res)")
    }
    
    func task1ToCountSum(fromYear: Int, toYear: Int, summa: Double) -> Double{
        let fromYear = 1826
        let toYear = 2019
        var sum = summa
        for _ in fromYear..<toYear{
            sum += sum * 0.06
        }
        return sum
    }
    
    func task2ToCountSumToLiveUptoNextYear(scholarship: Double, cosT: Double) -> Double{
        let studyYear = 10
        var cost = cosT
        var costs = 0.0
        var result = 0.0
        for _ in 0..<studyYear{
            costs += cost
            cost += cost * 0.03
            result += scholarship
        }
        costs -= result
        return costs
    }
    
    func task3ToCountMonths(scholarship: Double, cosT: Double, saving: Double) -> Double{
        var cost = cosT
        var result = saving
        var month = 0.0
        while result > cost{
            result += scholarship
            result -= cost
            cost += cost * 0.03
            month += 1
        }
        return month
    }
    
    func task4ReverseDigit(reversedDigit: Int) -> Int{
        var firsDigit = reversedDigit
        var reversedX = 0
        while firsDigit != 0 {
            reversedX = reversedX * 10 + firsDigit % 10
            firsDigit /= 10
            if (reversedX < 0 && firsDigit != 0 && Int(Int32.min) / 10 > reversedX ){
                firsDigit = 0
            }
            if (reversedX > 0 && firsDigit != 0 && Int(Int32.max) / 10 < reversedX ) {
                firsDigit = 0
            }
        }
        return reversedX
    }
    
}

